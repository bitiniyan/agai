# **AGAI**

It is a next-gen artificial intelligent enabler for smart agriculture.

Agai has been designed to help farmers to diagnose diseases in their crops and prescribe the right treatment along with possible causes and best practices. It has been built on AI and cloud technology and currently it is in the inception phase with minimal features like diagnosing 4 diseases for paddy.

We planning to expand further on paddy and other crop diseases. Also planing to empower the AI technology to identify seed variety and quality, classification of crops, livestock and bugs and pest, an Agri expert bot with speech recognition, forecasting diseases, localization with speech recognition to help uneducated farmers, and others.
In addition to that AI features we also plan to introduce other features like crop calendar, crops life cycle, livestock management, ERP, B2B options, etc.

## **Staus**

Currently this application under development.

## **Demo**

![wheather](/screenshots/gif.gif?raw=true)

## **Screen Shots of current state**

![crops](/screenshots/01.jpg?raw=true)
![wheather](/screenshots/02.jpg?raw=true)

## **How to Use?**

- **First Capture an image Of the Effected Crop Leaf By pressing the Snap button in the middle Of the Application.**

  ![wheather](/screenshots/2.png?raw=true)

  - **It will bring two options one is vhoose image from gallary another one directly capture image from your camera.**

    ![snap](/screenshots/3.png?raw=true)

  - **Once you choosen one of this option, it will sent the image to our agai ai platform.**

    ![diseases](/screenshots/4.png?raw=true)

  - **Agai ai platform start diagnosing disease. Currently it trained to detect four diseases of Paddy/Rice.**

    ![menu](/screenshots/5.png?raw=true)

  - **Once it succefully detect the disease it will directly take you to disease page where you can find all detail about diagnosed disease.**

    ![menu](/screenshots/6.png?raw=true)
