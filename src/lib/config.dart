class AppConfig {
  final String baseApiUrlDevelopment = 'https://agai-f307f.firebaseio.com/';
  final String baseApiUrlProduction = 'https://agai-f307f.firebaseio.com/';
  final String weatherAPI = "";
}

enum Environment {
  DEVELOPMENT,
  PRODUCTION,
}
