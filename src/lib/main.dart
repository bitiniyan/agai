import 'package:flutter/material.dart';

import './screens/snapshot-screen.dart';
import './screens/weather-page.dart';
import './screens/acknowledgement-page.dart';
import './screens/help-page.dart';
import './screens/settings-page.dart';
import './screens/disease-detail-page.dart';
import './screens/tabs_screen.dart';
import './screens/crop-wise-disease-details-page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Agai',
        theme: ThemeData(
          primarySwatch: Colors.pink,
          accentColor: Colors.amber,
          canvasColor: Color.fromRGBO(255, 254, 229, 1),
          fontFamily: 'Raleway',
          textTheme: ThemeData.light().textTheme.copyWith(
              bodyText1: TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              bodyText2: TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              headline1: TextStyle(
                fontSize: 40,
                fontFamily: 'RobotoCondensed',
                fontWeight: FontWeight.bold,
              ),
              headline2: TextStyle(
                fontSize: 35,
                fontFamily: 'RobotoCondensed',
                fontWeight: FontWeight.bold,
              ),
              headline3: TextStyle(
                fontSize: 30,
                fontFamily: 'RobotoCondensed',
                fontWeight: FontWeight.bold,
              ),
              headline4: TextStyle(
                fontSize: 25,
                fontFamily: 'RobotoCondensed',
                fontWeight: FontWeight.bold,
              ),
              headline5: TextStyle(
                fontSize: 20,
                fontFamily: 'RobotoCondensed',
                fontWeight: FontWeight.bold,
              )),
        ),
        initialRoute: '/',
        routes: {
          '/': (ctx) => TabsScreen(),
          CropWiseDiseaseListPage.routeName: (ctx) => CropWiseDiseaseListPage(),
          DiseaseDetailPage.routeName: (ctx) => DiseaseDetailPage(),
          SnapShotPage.routeName: (ctx) => SnapShotPage(),
          WeatherPage.routeName: (ctx) => WeatherPage(),
          SettingsPage.routeName: (ctx) => SettingsPage(),
          AcknowledgementPage.routeName: (ctx) => AcknowledgementPage(),
          HelpPage.routeName: (ctx) => HelpPage(),
        });
  }
}
