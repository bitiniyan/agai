import 'package:rxdart/rxdart.dart';
import '../models/disease-detail.dart';
import '../models/diseases.dart';
import '../data/disease-repository.dart';

class DiseaseBloc {
  final _repository = DiseaseRepository();
  final _fetcherList = PublishSubject<List<DiseaseModel>>();
  final _fetcherDetail = PublishSubject<DiseaseDetailModel>();

  Stream<List<DiseaseModel>> get diseaseList => _fetcherList.stream;
  Stream<DiseaseDetailModel> get diseaseDetail => _fetcherDetail.stream;

  fetchDiseases(String cropId) async {
    List<DiseaseModel> acitveDiseaseList =
        await _repository.fetchDiseaseList(cropId);
    _fetcherList.sink.add(acitveDiseaseList);
  }

  fetchDiseaseDetail(String cropId, String diseaseId) async {
    DiseaseDetailModel diseaseDetail =
        await _repository.fetchDiseaseById(cropId, diseaseId);
    _fetcherDetail.sink.add(diseaseDetail);
  }

  dispose() {
    _fetcherList.close();
    _fetcherDetail.close();
  }
}

final bloc = DiseaseBloc();
