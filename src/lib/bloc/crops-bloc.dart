import 'package:rxdart/rxdart.dart';
import '../data/crops-repository.dart';
import '../models/crops.dart';

class CropsBloc {
  final _repository = CropsRepository();
  final _fetcher = PublishSubject<List<CropsModel>>();

  Stream<List<CropsModel>> get activeCrops => _fetcher.stream;

  fetchCrops() async {
    List<CropsModel> activeCrops = await _repository.fetchCropList();
    _fetcher.sink.add(activeCrops);
  }

  dispose() {
    _fetcher.close();
  }
}

final bloc = CropsBloc();
