import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../widgets/disease-detail.dart';
import '../models/disease-detail.dart';
import '../bloc/disease-bloc.dart';

class DiseaseDetailPage extends StatefulWidget {
  static const String routeName = "/disease-details";

  @override
  _DiseaseDetailPageState createState() => _DiseaseDetailPageState();
}

class _DiseaseDetailPageState extends State<DiseaseDetailPage> {
  String cropId;
  String id;
  String name;
  @override
  void didChangeDependencies() {
    final routeArgs =
        ModalRoute.of(context).settings.arguments as Map<String, String>;
    cropId = routeArgs["cropId"];
    id = routeArgs["id"];
    name = routeArgs["name"];
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    bloc.fetchDiseaseDetail(cropId, id);
    return StreamBuilder(
      stream: bloc.diseaseDetail,
      builder: (context, AsyncSnapshot<DiseaseDetailModel> snapshot) {
        if (snapshot.hasData) {
          return _createUIView(snapshot);
        } else if (snapshot.hasError) {
          return Text(snapshot.error.toString());
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }

  Widget _createUIView(AsyncSnapshot<DiseaseDetailModel> snapshot) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text(name),
      // ),
      // body: Container(
      //     child: SingleChildScrollView(
      //         child: DiseaseDetailWidget(snapshot.data)))

      body: Container(
          decoration: BoxDecoration(color: Colors.white70),
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Container(
                      child: Hero(
                        tag: cropId,
                        child: ClipRRect(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(60.0)),
                            child: ColorFiltered(
                              colorFilter: ColorFilter.mode(
                                  Colors.black.withOpacity(0.3),
                                  BlendMode.overlay),
                              child: CachedNetworkImage(
                                imageUrl: snapshot.data.imageUrl,
                                fit: BoxFit.cover,
                              ),
                            )),
                      ),
                    ),
                    Positioned(
                      top: MediaQuery.of(context).padding.top,
                      child: IconButton(
                        icon: Icon(Icons.arrow_back),
                        color: Colors.white,
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                  ],
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.fromLTRB(16, 5, 5, 0),
                          child: Text(
                            snapshot.data.name,
                            style: Theme.of(context).textTheme.headline4,
                          )),
                      Container(
                        decoration: BoxDecoration(
                            color: Color(0xFFE3E3E3),
                            borderRadius: BorderRadius.circular(10)),
                        padding: EdgeInsets.fromLTRB(8, 4, 8, 4),
                        margin: EdgeInsets.all(16),
                        child: Text(
                          "diseases, paddy",
                          style: TextStyle(color: Colors.black, fontSize: 20),
                        ),
                      ),
                      Padding(
                          padding: EdgeInsets.fromLTRB(16, 5, 5, 0),
                          child: Text(
                            "Symptoms",
                            style: Theme.of(context).textTheme.headline5,
                          )),
                      Container(
                        child: Padding(
                            padding: EdgeInsets.fromLTRB(16, 5, 16, 0),
                            child: Text(
                              snapshot.data.symptoms,
                              textAlign: TextAlign.justify,
                              style: TextStyle(
                                fontSize: 18,
                              ),
                            )),
                      ),
                      Padding(
                          padding: EdgeInsets.fromLTRB(16, 15, 5, 5),
                          child: Text(
                            "Reason",
                            style: Theme.of(context).textTheme.headline5,
                          )),
                      Container(
                        child: Padding(
                            padding: EdgeInsets.fromLTRB(16, 5, 16, 0),
                            child: Text(
                              snapshot.data.reason,
                              textAlign: TextAlign.justify,
                              style: TextStyle(
                                fontSize: 18,
                              ),
                            )),
                      ),
                      Padding(
                          padding: EdgeInsets.fromLTRB(16, 15, 5, 5),
                          child: Text(
                            "Treatment - Biological",
                            style: Theme.of(context).textTheme.headline5,
                          )),
                      Container(
                        child: Padding(
                            padding: EdgeInsets.fromLTRB(16, 5, 16, 0),
                            child: Text(
                              snapshot.data.bioLogicalTreatment,
                              textAlign: TextAlign.justify,
                              style: TextStyle(
                                fontSize: 18,
                              ),
                            )),
                      ),
                      Padding(
                          padding: EdgeInsets.fromLTRB(16, 15, 5, 5),
                          child: Text(
                            "Treatment - Chimical",
                            style: Theme.of(context).textTheme.headline5,
                          )),
                      Container(
                        child: Padding(
                            padding: EdgeInsets.fromLTRB(16, 5, 16, 0),
                            child: Text(
                              snapshot.data.chemicalTreatment,
                              textAlign: TextAlign.justify,
                              style: TextStyle(
                                fontSize: 18,
                              ),
                            )),
                      ),
                      Padding(
                          padding: EdgeInsets.fromLTRB(16, 15, 5, 5),
                          child: Text(
                            "Preventive Measures",
                            style: Theme.of(context).textTheme.headline5,
                          )),
                      _createPreventiveMeasureView(
                          snapshot.data.preventiveMeasures),
                      SizedBox(height: 50)
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }

  Widget _createPreventiveMeasureView(List<String> data) {
    List<Widget> widgets = List<Widget>();
    data.forEach((item) {
      widgets.add(Container(
        child: Padding(
            padding: EdgeInsets.fromLTRB(16, 5, 16, 5),
            child: Text(
              item,
              textAlign: TextAlign.left,
              style: TextStyle(
                fontSize: 18,
              ),
            )),
      ));
    });
    return Column(
      children: widgets,
    );
  }
}
