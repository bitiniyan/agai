import 'package:flutter/material.dart';

class WeatherPage extends StatelessWidget {
  static const String routeName = "/weather";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Weather'),
        ),
        body: Center(
            child: Text(
          '(development in progress)',
          style: Theme.of(context).textTheme.headline5,
        )));
  }
}
