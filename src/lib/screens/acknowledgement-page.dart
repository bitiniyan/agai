import 'package:flutter/material.dart';

class AcknowledgementPage extends StatelessWidget {
  static const String routeName = "/acknowledge";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Acknowledgement'),
        ),
        body: Center(
            child: Text(
          '(development in progress)',
          style: Theme.of(context).textTheme.headline5,
        )));
  }
}
