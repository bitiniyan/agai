import 'package:flutter/material.dart';

class SnapShotPage extends StatelessWidget {
  static const String routeName = "/snapshot";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Snap a crop'),
        ),
        body: Center(
            child: Text(
          'Snap a crop to detect the disease\n (development in progress)',
          style: Theme.of(context).textTheme.headline5,
        )));
  }
}
