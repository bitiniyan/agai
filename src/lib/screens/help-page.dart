import 'package:flutter/material.dart';

class HelpPage extends StatelessWidget {
  static const String routeName = "/help";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Help'),
        ),
        body: Center(
            child: Text(
          '(development in progress)',
          style: Theme.of(context).textTheme.headline5,
        )));
  }
}
