import 'package:flutter/material.dart';

import '../widgets/disease-list.dart';
import '../models/diseases.dart';
import '../bloc/disease-bloc.dart';

class CropWiseDiseaseListPage extends StatefulWidget {
  static const routeName = "/crop-details";

  @override
  _CropWiseDiseaseListPageState createState() =>
      _CropWiseDiseaseListPageState();
}

class _CropWiseDiseaseListPageState extends State<CropWiseDiseaseListPage> {
  String id;
  String name;
  String imageUrl;
  List<DiseaseModel> diseases;

  @override
  void didChangeDependencies() {
    final routeArgs =
        ModalRoute.of(context).settings.arguments as Map<String, String>;
    id = routeArgs["id"];
    name = routeArgs["name"];
    imageUrl = "assets/images/canvas/$id.jpg";

    super.didChangeDependencies();
  }

  Widget _createUIView(AsyncSnapshot<List<DiseaseModel>> snapshot) {
    var diseases = snapshot.data;
    return Scaffold(
        appBar: AppBar(
          title: Text(name),
        ),
        body: ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemBuilder: (ctx, index) {
            if (index == 0) {
              return DiseaseListWidget(
                id: diseases[index].id,
                name: diseases[index].name,
                imageUrl: diseases[index].imageUrl,
                cropId: id,
                cropName: name,
                includeHeader: true,
              );
            }

            return DiseaseListWidget(
              id: diseases[index].id,
              name: diseases[index].name,
              imageUrl: diseases[index].imageUrl,
              cropId: id,
              cropName: name,
            );
          },
          itemCount: snapshot.data.length,
        ));
  }

  @override
  Widget build(BuildContext context) {
    bloc.fetchDiseases(id);
    return StreamBuilder(
      stream: bloc.diseaseList,
      builder: (context, AsyncSnapshot<List<DiseaseModel>> snapshot) {
        if (snapshot.hasData) {
          return _createUIView(snapshot);
        } else if (snapshot.hasError) {
          return Text(snapshot.error.toString());
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }
}
