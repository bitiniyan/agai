import 'package:flutter/material.dart';

import '../models/crops.dart';
import '../widgets/crop-item.dart';
import '../bloc/crops-bloc.dart';

class CropsPage extends StatelessWidget {
  static const routeName = '/crops';
  @override
  Widget build(BuildContext context) {
    bloc.fetchCrops();
    return StreamBuilder(
      stream: bloc.activeCrops,
      builder: (context, AsyncSnapshot<List<CropsModel>> snapshot) {
        if (snapshot.hasData) {
          return _createGridView(snapshot);
        } else if (snapshot.hasError) {
          return Text(snapshot.error.toString());
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }

  Widget _createGridView(AsyncSnapshot<List<CropsModel>> snapshot) {
    return Stack(
      children: <Widget>[
        Container(
          child: CustomPaint(
            painter: ShapesPainter(),
            child: Container(
              height: 300,
            ),
          ),
        ),
        Container(
          //padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 2.0),
          child: GridView.count(
            crossAxisCount: 2,
            children: snapshot.data
                .map(
                  (cropModel) => CropItemWidget(cropModel.id, cropModel.name),
                )
                .toList(),
          ),
        ),
      ],
    );
  }
}

class ShapesPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint();
    // set the paint color to be white
    paint.color = Colors.white;
    // Create a rectangle with size and width same as the canvas
    var rect = Rect.fromLTWH(0, 0, size.width, size.height);
    // draw the rectangle using the paint
    canvas.drawRect(rect, paint);
    paint.color = Colors.greenAccent[400];
    // create a path
    var path = Path();
    path.lineTo(0, size.height);
    path.lineTo(size.width, 0);
    // close the path to form a bounded shape
    path.close();
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
