import 'package:http/http.dart' show Client;

import 'dart:convert';

import '../models/disease-detail.dart';
import '../models/diseases.dart';
import '../models/crops.dart';
import '../config.dart';

class AgaiDataProvider {
  AppConfig config = AppConfig();
  String url;
  Client client = Client();

  AgaiDataProvider() {
    url = config.baseApiUrlProduction;
  }

  static AgaiDataProvider _instance;
  static AgaiDataProvider getInstance() => _instance ??= AgaiDataProvider();

  Future<List<CropsModel>> getCrops() async {
    var crops = List<CropsModel>();
    var map = await _apiCall('$url/crops.json');
    map.forEach((key, value) {
      var cropsModel = CropsModel.deserialize(MapEntry(key, value));
      crops.add(cropsModel);
    });
    crops.sort((a, b) => a.order.compareTo(b.order));
    return crops;
  }

  Future<List<DiseaseModel>> getDiseasesByCrop(String cropId) async {
    var crops = List<DiseaseModel>();
    var map = await _apiCall('$url/diseases/$cropId.json');
    map.forEach((key, value) {
      var diseaseModel = DiseaseModel.deserialize(MapEntry(key, value), cropId);
      crops.add(diseaseModel);
    });
    return crops;
  }

  Future<DiseaseDetailModel> getDiseasesById(
      String cropId, String diseaseId) async {
    var map = await _apiCall('$url/diseases/$cropId/$diseaseId.json');

    return DiseaseDetailModel.deserialize(map, cropId);
  }

  Future<Map<String, dynamic>> _apiCall(String url) async {
    var response = await client
        .get(Uri.encodeFull(url), headers: {'Accept': 'application/json'});
    JsonDecoder decoder = JsonDecoder();
    return decoder.convert(response.body) as Map<String, dynamic>;
  }
}
