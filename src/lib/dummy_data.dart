import './models/diseases.dart';
import './models/crops.dart';

const DUMMY_CROPS = const [
  CropsModel(
      id: 'paddy',
      name: 'Paddy/Rice',
      imageUrl: 'assets/images/crops/paddy.png'),
  CropsModel(
      id: 'tomato', name: 'Tomato', imageUrl: 'assets/images/crops/onion.png'),
  CropsModel(
      id: 'potato', name: 'Potato', imageUrl: 'assets/images/crops/cotton.png'),
  CropsModel(
      id: 'onion', name: 'Onion', imageUrl: 'assets/images/crops/onion.png'),
  CropsModel(
      id: 'cotton', name: 'Cotton', imageUrl: 'assets/images/crops/cotton.png'),
  CropsModel(
      id: 'maize', name: 'Maize', imageUrl: 'assets/images/crops/maize.png'),
  CropsModel(
      id: 'sugarcane',
      name: 'SugarCane',
      imageUrl: 'assets/images/crops/sugarcane.png'),
  CropsModel(
      id: 'peanut', name: 'Peanut', imageUrl: 'assets/images/crops/peanut.png'),
  CropsModel(
      id: 'chilli', name: 'Chilli', imageUrl: 'assets/images/crops/chilli.png'),
  CropsModel(
      id: 'mango', name: 'Mango', imageUrl: 'assets/images/crops/mango.png'),
  CropsModel(
      id: 'banana', name: 'Banana', imageUrl: 'assets/images/crops/banana.png'),
  CropsModel(
      id: 'bittergourd',
      name: 'Bitter Gourd',
      imageUrl: 'assets/images/crops/banana.png'),
];

const DUMMY_DISEASES = const [
  DiseaseModel(
    id: 'bacterial_blight',
    name: 'Bacterial Bligh',
    imageUrl:
        'http://www.knowledgebank.irri.org/media/zoo/images/pests-teaser-bacterial-leaf-blight_21b8e43b1d8dfb6568fae7078cbd20b9.jpg',
  ),
  DiseaseModel(
    id: 'bacterial_leaf_streak',
    name: 'Bacterial Leaf Streak',
    imageUrl:
        'http://www.knowledgebank.irri.org/media/zoo/images/pests-teaser-bacterial-leaf-streak_90912b7ac4bbb2f6c56e4d03f6242e15.jpg',
  ),
  DiseaseModel(
    id: 'rice_blast',
    name: 'Rice Blast',
    imageUrl:
        'http://www.knowledgebank.irri.org/media/zoo/images/pests-teaser-blast_3abfdfdd8ba6c20a4d55a09373fb09be.jpg',
  ),
  DiseaseModel(
    id: 'brown_spot',
    name: 'Brown Spot',
    imageUrl:
        'http://www.knowledgebank.irri.org/media/zoo/images/pests-teaser-brown-spot_498e15e546b4e17d0f84ac42cab1d3f1.jpg',
  ),
  DiseaseModel(
    id: 'rice_blast',
    name: 'Rice Blast',
    imageUrl:
        'http://www.knowledgebank.irri.org/media/zoo/images/pests-teaser-blast_3abfdfdd8ba6c20a4d55a09373fb09be.jpg',
  ),
  DiseaseModel(
    id: 'brown_spot',
    name: 'Brown Spot',
    imageUrl:
        'http://www.knowledgebank.irri.org/media/zoo/images/pests-teaser-brown-spot_498e15e546b4e17d0f84ac42cab1d3f1.jpg',
  ),
  DiseaseModel(
    id: 'rice_blast',
    name: 'Rice Blast',
    imageUrl:
        'http://www.knowledgebank.irri.org/media/zoo/images/pests-teaser-blast_3abfdfdd8ba6c20a4d55a09373fb09be.jpg',
  ),
  DiseaseModel(
    id: 'brown_spot',
    name: 'Brown Spot',
    imageUrl:
        'http://www.knowledgebank.irri.org/media/zoo/images/pests-teaser-brown-spot_498e15e546b4e17d0f84ac42cab1d3f1.jpg',
  )
];
