class DiseaseDetailModel {
  final String id;
  final String name;
  final String imageUrl;
  final String cropId;
  final List<String> preventiveMeasures;
  final String reason;
  final String symptoms;
  final String bioLogicalTreatment;
  final String chemicalTreatment;
  const DiseaseDetailModel(
      {this.id,
      this.name,
      this.imageUrl,
      this.cropId,
      this.preventiveMeasures,
      this.reason,
      this.symptoms,
      this.bioLogicalTreatment,
      this.chemicalTreatment});

  static deserialize(Map<String, dynamic> map, String cropId) {
    String id = map['key'];
    String name = map['name'];
    String imageUrl = map['image_url'];
    var preventiveMeasures = map['preventive_measure'] as List;
    var measures = preventiveMeasures.cast<String>();

    String reason = map['reason'];
    String symptoms = map['symptoms'];
    String bioLogicalTreatment = map['treatments']['biological'];
    String chemicalTreatment = map['treatments']['chemical'];

    return DiseaseDetailModel(
        id: id,
        name: name,
        imageUrl: imageUrl,
        cropId: cropId,
        preventiveMeasures: measures,
        reason: reason,
        symptoms: symptoms,
        bioLogicalTreatment: bioLogicalTreatment,
        chemicalTreatment: chemicalTreatment);
  }
}
