import 'package:flutter/material.dart';

class CropsModel {
  final String id;
  final String name;
  final String imageUrl;
  final int order;
  const CropsModel(
      {@required this.id, @required this.name, this.imageUrl, this.order});

  static CropsModel deserialize(MapEntry map) {
    String id = map.key;
    String name = map.value['name'];
    int order = map.value['order'].toInt();
    return CropsModel(id: id, name: name, order: order);
  }
}
