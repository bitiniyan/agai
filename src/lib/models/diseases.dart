class DiseaseModel {
  final String id;
  final String name;
  final String imageUrl;
  final String cropId;

  const DiseaseModel({this.id, this.name, this.imageUrl, this.cropId});

  static deserialize(MapEntry<String, dynamic> map, String cropId) {
    String id = map.key;
    String name = map.value['name'];
    String imageUrl = map.value['image_url'];

    return DiseaseModel(id: id, name: name, imageUrl: imageUrl, cropId: cropId);
  }
}
