import 'package:flutter/material.dart';
import '../screens/crop-wise-disease-details-page.dart';

class CropItemWidget extends StatelessWidget {
  final String id;
  final String name;

  CropItemWidget(this.id, this.name);

  void _selectedCrop(BuildContext context) {
    if (id != "paddy") return;
    Navigator.of(context).pushNamed(
      CropWiseDiseaseListPage.routeName,
      arguments: {
        'id': id,
        'name': name,
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return makeDashboardItem(name, Icons.account_box, context);
    // return Container(
    //   width: 300,
    //   height: 300,
    //   child: CircleButton(
    //     title: name,
    //     size: 40,
    //     iconData: Icons.access_alarm,
    //     onTap: () => _selectedCrop(context),
    //   ),
    //   decoration: BoxDecoration(
    //       color: Color.fromRGBO(220, 220, 220, 1.0), shape: BoxShape.circle),
    // );
  }

  Widget makeDashboardItem(String title, IconData icon, BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(10),
        // elevation: 10,
        // color: Colors.blue,
        // shape: RoundedRectangleBorder(
        //   borderRadius: BorderRadius.all(Radius.circular(20)),
        //   side: BorderSide(color: Colors.white),
        // ),
        child: Container(
          width: 50,
          height: 50,
          decoration: BoxDecoration(
              color: id == "paddy" ? Colors.white : Colors.grey,
              // color: RandomColor().randomColor(
              //     colorBrightness: ColorBrightness.light,
              //     colorHue: ColorHue.green,
              //     colorSaturation: ColorSaturation.mediumSaturation),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.4),
                  spreadRadius: 7,
                  blurRadius: 7,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
              //borderRadius: BorderRadius.circular(20),
              shape: BoxShape.circle),
          child: new InkWell(
            onTap: () {
              _selectedCrop(context);
            },
            child: Column(
              // crossAxisAlignment: CrossAxisAlignment.stretch,
              // mainAxisSize: MainAxisSize.min,
              // verticalDirection: VerticalDirection.down,
              children: <Widget>[
                SizedBox(height: 30.0),
                Center(
                  child: SizedBox(
                      child: Image.asset(
                    "assets/images/$id.png",
                  )),
                ),
                SizedBox(height: 10.0),
                new Center(
                  child: new Text(title,
                      style:
                          new TextStyle(fontSize: 18.0, color: Colors.black)),
                )
              ],
            ),
          ),
        ));
  }
}
