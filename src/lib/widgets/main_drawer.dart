import 'package:flutter/material.dart';

import '../screens/weather-page.dart';
import '../screens/acknowledgement-page.dart';
import '../screens/help-page.dart';
import '../screens/settings-page.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, Function tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 80,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Theme.of(context).accentColor,
            child: Text(
              'Agai',
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 35,
                  color: Theme.of(context).primaryColor),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          buildListTile('Home', Icons.home, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Crops', Icons.image, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Weather', Icons.wb_sunny, () {
            Navigator.of(context).pushReplacementNamed(WeatherPage.routeName);
          }),
          buildListTile('Settings', Icons.settings, () {
            Navigator.of(context).pushReplacementNamed(SettingsPage.routeName);
          }),
          buildListTile('Acknowledgement', Icons.people, () {
            Navigator.of(context)
                .pushReplacementNamed(AcknowledgementPage.routeName);
          }),
          buildListTile('Help', Icons.help, () {
            Navigator.of(context).pushReplacementNamed(HelpPage.routeName);
          }),
          buildListTile('AboutUs', Icons.contact_mail, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
        ],
      ),
    );
  }
}
