import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import '../screens/disease-detail-page.dart';

class DiseaseListWidget extends StatelessWidget {
  final String id;
  final String name;
  final String imageUrl;
  final String cropId;
  final String cropName;
  final bool includeHeader;

  DiseaseListWidget(
      {this.id,
      this.name,
      this.imageUrl,
      this.cropId,
      this.cropName,
      this.includeHeader = false});

  void _selectedDisease(BuildContext context) {
    Navigator.of(context).pushNamed(
      DiseaseDetailPage.routeName,
      arguments: {
        'cropId': cropId,
        'id': id,
        'name': name,
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    if (includeHeader) {
      return Column(
        children: <Widget>[
          _buildHeaderImage(context),
          _buildWidgetView(context)
        ],
      );
    }
    return _buildWidgetView(context);
  }

  Widget _buildWidgetView(BuildContext context) {
    return InkWell(
      onTap: () => {_selectedDisease(context)},
      child: Container(
        height: 150,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
          elevation: 4,
          margin: EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: CachedNetworkImage(
                      imageUrl: imageUrl,
                      height: 130,
                      width: double.infinity,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Positioned(
                    bottom: 20,
                    right: 10,
                    child: Container(
                      width: 300,
                      color: Colors.black54,
                      padding: EdgeInsets.symmetric(
                        vertical: 5,
                        horizontal: 20,
                      ),
                      child: Text(
                        name,
                        style: TextStyle(
                          fontSize: 26,
                          color: Colors.white,
                        ),
                        softWrap: true,
                        overflow: TextOverflow.fade,
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildSectionTitle(BuildContext context, String text) {
    return Container(
      child: Text(
        text,
        style: Theme.of(context).textTheme.headline4,
      ),
    );
  }

  Widget _buildHeaderImage(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: 250,
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30),
            ),
            elevation: 4,
            margin: EdgeInsets.all(10),
            child: Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                      ),
                      child: Image.asset(
                        'assets/images/canvas/$cropId.jpg',
                        height: 230,
                        width: double.infinity,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Positioned(
                      bottom: 20,
                      right: 10,
                      child: Container(
                        width: 300,
                        color: Colors.black54,
                        padding: EdgeInsets.symmetric(
                          vertical: 5,
                          horizontal: 20,
                        ),
                        child: Text(
                          cropName,
                          style: TextStyle(
                            fontSize: 26,
                            color: Colors.white,
                          ),
                          softWrap: true,
                          overflow: TextOverflow.fade,
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
        Container(
          //margin: EdgeInsets.symmetric(vertical: 10),
          child: Text(
            "Diseases",
            style: Theme.of(context).textTheme.headline4,
          ),
        )
      ],
    );
  }
}
