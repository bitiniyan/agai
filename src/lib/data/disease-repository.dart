import '../models/disease-detail.dart';
import '../models/diseases.dart';
import '../providers/agai-data-provider.dart';

class DiseaseRepository {
  final provider = AgaiDataProvider();
  Future<List<DiseaseModel>> fetchDiseaseList(String cropId) =>
      provider.getDiseasesByCrop(cropId);

  Future<DiseaseDetailModel> fetchDiseaseById(
          String cropId, String diseaseId) =>
      provider.getDiseasesById(cropId, diseaseId);
}
