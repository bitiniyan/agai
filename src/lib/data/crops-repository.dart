import '../models/crops.dart';
import '../providers/agai-data-provider.dart';

class CropsRepository {
  final provider = AgaiDataProvider();
  Future<List<CropsModel>> fetchCropList() => provider.getCrops();
}
